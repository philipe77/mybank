import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
 // CameraPosition,
 // MarkerOptions,
  Marker
} from '@ionic-native/google-maps';


import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  map: GoogleMap;
  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    this.loadMap();
  }
  loadMap() {

    let mapOptions: GoogleMapOptions = {
     controls:{
      'compass': false,
      'myLocationButton': true,
      'myLocation': true,
     },
      camera: {
         target: {
           lat: -22.923894,
           lng: -43.2303857
         },
         
         zoom: 18,
         tilt: 30
       }
    };

    this.map = GoogleMaps.create('map', mapOptions);

    this.map.addMarker ({
      title: 'ITAU',
      disableAutoPan: true,
      icon: 'green',
      animation: 'DROP',
      position: {
        lat: -22.9293263,
        lng: -43.239017
      }
    }).then(this.onMarkerAdded);
    
    this.map.addMarker ({
      title: 'ITAU',
      disableAutoPan: true,
      icon: 'green',
      animation: 'DROP',
      position: {
        lat: -22.9301768,
        lng: -43.2401567
      }
    }).then(this.onMarkerAdded);  
  }

  
  
  onMarkerAdded(marker: Marker) {
    marker.one(GoogleMapsEvent.MARKER_CLICK).then(() => {
      alert("Marker " + marker.getTitle() + " is clicked");
    });
  }

}
