import { Component, OnInit } from '@angular/core';
import { NavController } from 'ionic-angular';
import { BancoService } from '../../app/shared/banco.service';
import { Banco } from '../../app/shared/banco.model';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class ContactPage {

 // bancos:Banco[];
  constructor(public navCtrl: NavController,
              private bancoService:BancoService) {
                this.generateTopics();
  }

  topics:string[];

  ionViewDidLoad(){
    
    //this.bancos =this.bancoService.getBancos();
  }

  generateTopics(){
    this.topics=[
        "ITAU",
        "Banco do Brasil",
        "Santander",
        "Bradesco"
    ]
}

  getBancos(ev:any){
    console.log(this.topics)
    this.generateTopics();
    let serVal = ev.target.value;

    if(serVal && serVal.trim() != ''){
        this.topics = this.topics.filter((topics)=>{
            return (topics.toLowerCase().indexOf(serVal.toLowerCase()) >-1);
        })
    }
}

  

}
