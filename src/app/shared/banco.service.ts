import { Banco } from "./banco.model";
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'
import { AutoCompleteService } from 'ionic2-auto-complete';
import {Injectable} from "@angular/core";


@Injectable()
export class BancoService{ //implements AutoCompleteService{

    private bancos:Banco[] = [
        new Banco("Itau"), //-22.9309082, -43.2430135,
        new Banco("Santander"), //-22.9313135, -43.2382922),
        new Banco("Banco do Brasil"),// -22.9268976,-43.2357802),
        new Banco("Bradesco")// -22.9285624,-43.2379196)
    ]

    labelAttribute = "name";

    

    

    generateBancos(){
        return this.bancos;
    }




/* 
    getResults(keyword:string) {
        return this.http.get("https://restcountries.eu/rest/v1/name/"+keyword)
          .map(
            result =>
            {
              return result.json()
                .filter(item => item.name.toLowerCase().startsWith(keyword.toLowerCase()) )
            });
      } */
    
}